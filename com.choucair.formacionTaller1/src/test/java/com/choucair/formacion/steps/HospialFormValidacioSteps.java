package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.HospitalFormValidacionPage;

import net.thucydides.core.annotations.Step;

public class HospialFormValidacioSteps {
	HospitalFormValidacionPage hospitalFormValidacionPage;
	@Step
	public void diligenciar_popup_datos_tabla(List<List<String>> data, int id){
		//Doctor
		hospitalFormValidacionPage.Nombre_Requerido(data.get(id).get(0).trim());
		hospitalFormValidacionPage.Apellido_Requerido(data.get(id).get(1).trim());
		hospitalFormValidacionPage.Telefono(data.get(id).get(2).trim());
		hospitalFormValidacionPage.Selector_Documento(data.get(id).get(3).trim());
		hospitalFormValidacionPage.Numero_Documento(data.get(id).get(4).trim());

	}
	@Step
	public void verificar_ingreso_datos_formulario_exitoso() {
		hospitalFormValidacionPage.GardarDoctor();
		try {
			Thread.sleep(2000);
			hospitalFormValidacionPage.Inicio();

		} catch (InterruptedException e) {
		
		}

	}
	@Step
	public void diligenciar_popup_datos_tabla2(List<List<String>> data, int id){
		
		//Paciente
		hospitalFormValidacionPage.Nombre_Requerido_Paciente(data.get(id).get(0).trim());
		hospitalFormValidacionPage.Apellido_Requerido_Paciente(data.get(id).get(1).trim());
		hospitalFormValidacionPage.Telefono_Paciente(data.get(id).get(2).trim());
		hospitalFormValidacionPage.Selector_Documento_Paciente(data.get(id).get(3).trim());
		hospitalFormValidacionPage.Numero_Documento_Paciente(data.get(id).get(4).trim());
		hospitalFormValidacionPage.Salud_Prepagada();
	}
	@Step
	public void verificar_ingreso_datos_formulario_exitoso2() {
		hospitalFormValidacionPage.GardarPaciente();
	}
	
	@Step
	public void diligenciar_popup_datos_tabla3(List<List<String>> data, int id){
		
		//Agendar Cita
		hospitalFormValidacionPage.Fecha_Cita(data.get(id).get(0).trim());
		hospitalFormValidacionPage.Cerrar_Calendario();
		hospitalFormValidacionPage.Docum_Paciente(data.get(id).get(1).trim());
		hospitalFormValidacionPage.Docum_Doctor(data.get(id).get(2).trim());
		hospitalFormValidacionPage.Observaciones(data.get(id).get(3).trim());
		
	
	}
	@Step
	public void verificar_ingreso_datos_formulario_exitoso3() {
		hospitalFormValidacionPage.GardarCita();
	}
}
